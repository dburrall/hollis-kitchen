											Holli's Kitchen
											----------------
											
* A website I made for my wife, inspired by her cooking. You'll find a number of recipes that shes made that I had to share with the world*
====================
This project uses:
1.HTML
2.CSS
3.JS for the search bar
4.Bootstrap
5.Google fonts
=================

This is a public repository for anyone to look at and use. Images are not my own and are from free images scattered throughout the internet. Feel free to clone and enjoy!