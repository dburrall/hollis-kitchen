var $ = jQuery;
$( function() {
    var availableTags = [
      {value:"cheese tortellini", link:'https://elaborate-mermaid-81d286.netlify.app/cheesetort'},
      {value:"chicken picata",link:'https://elaborate-mermaid-81d286.netlify.app/chickenpicata'},
      {value:"corn salsa",link:'https://elaborate-mermaid-81d286.netlify.app/cornsalsa'},
      {value:"curry veggie bowl",link:'https://elaborate-mermaid-81d286.netlify.app/curryveggiebowl'},
      {value:"fajitas",link:'https://elaborate-mermaid-81d286.netlify.app/fajitas.html'},
      {value:"maple thighs",link:'https://elaborate-mermaid-81d286.netlify.app/maplethighs'},
      {value:"nachos",link:'https://elaborate-mermaid-81d286.netlify.app/nachos'},
      {value:"putanesca",link:'https://elaborate-mermaid-81d286.netlify.app/putanesca'},
      {value:"reueben dip",link:'https://google.com'},
      {value:"shrimp and sausage",link:'https://elaborate-mermaid-81d286.netlify.app/shrimp&sausage.html'},
      {value:"tacos pastor",link:'https://elaborate-mermaid-81d286.netlify.app/tacospastor'},
      {value:"veggie pasta",link:'https://elaborate-mermaid-81d286.netlify.app/veggiepasta'},
      
    ];
    $( "#inputSearch" ).autocomplete({
      source: availableTags,
      select: function( event, ui ) {
        console.log(ui.item.link);
        window.location.replace(ui.item.link)
      }
    });
  } );